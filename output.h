#ifndef	_OUTPUT_H
#define	_OUTPUT_H	1

#include <stdio.h>
#include "analysis.h"

#define	OUTPUT_NODE_PARAMS	\
	struct Light *light, FILE *stream, int level

void lightscript_output_node(OUTPUT_NODE_PARAMS, int colored);

#endif	// _OUTPUT_H
