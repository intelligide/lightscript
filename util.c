#include "util.h"

void rprint(reference_t ref, FILE *stream)
{
	const char *ptr = ref.begin;
	while(ptr < ref.end) fputc(*ptr++, stream);
}

void rprint_indent(reference_t ref, FILE *stream, int indent)
{
	const char *ptr = ref.begin;

	while(1)
	{
		ptr = line_skipIndent(ptr, ref.end, indent);
		while(ptr < ref.end && *ptr != '\n') fputc(*ptr++, stream);

		if(ptr >= ref.end) return;
		putchar(*ptr++);
	}
}

const char *line_next(const char *ptr, const char *limit)
{
	while(ptr < limit && *ptr != '\n') ptr++;
	if(ptr >= limit) return limit;
	return ptr + 1;
}

int line_isEmpty(const char *ptr, const char *limit)
{
	while(ptr < limit && *ptr != '\n')
	{
		if(!isspace(*ptr)) return 0;
		ptr++;
	}
	return 1;
}

int line_endsWith(const char *line, const char *line_end, int character,
	int count)
{
	// Skipping spaces at the end of the line.
	while(--line_end > line && isspace(*line_end));

	if(line_end - line < count - 1) return 0;
	while(count--) if(*line_end-- != character) return 0;
	return 1;
}

int line_indent(const char *line, const char *limit)
{
	int level = 0;

	while(line < limit)
	{
		if(*line == '\t') level++;
		else if(line < limit - 1 && *line == ' ' && *(line + 1) == ' ')
		{
			level++;
			line++;
		}
		else return level;

		line++;
	}
	return level;
}

const char *line_skipIndent(const char *line, const char *limit, int levels)
{
	while(line < limit && levels)
	{
		if(*line == '\t') levels--;
		else if(line < limit - 1 && *line == ' ' && *(line + 1) == ' ')
		{
			levels--;
			line++;
		}
		else return line;

		line++;
	}
	return line;
}
