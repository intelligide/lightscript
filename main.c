#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lightscript.h"

struct Options
{
	int stats;
	int verbose;
	int colors;
};

struct Stats
{
	clock_t begin;
	clock_t inter;
	clock_t output;
	clock_t end;
};

struct Options	parse_args(int argc, char **argv);
struct Stats	light(const char *filename, struct Options options);

void statistics(struct Stats stats);
void stop(void);

struct Options parse_args(int argc, char **argv)
{
	struct Options o = { 0, 0, 0 };
	char *arg;
	int i;
	int erase = 0;

	for(i = 1; i < argc; i++)
	{
		arg = argv[i];
		erase = 1;

		if(!strcmp(arg, "-s") || !strcmp(arg, "--stats")) o.stats = 1;
		else if(!strcmp(arg, "-v")) o.verbose = 1;
		else if(!strcmp(arg, "-c")) o.colors = 1;

		else erase = 0;
		// Keeping only file arguments.
		if(erase) argv[i] = NULL;
	}

	return o;
}

struct Stats light(const char *filename, struct Options options)
{
	struct Stats stats = { 0, 0, 0, 0 };
	struct Light *light;
	unsigned long size;
	char *pointer;
	FILE *fp;
	int x;

	stats.begin = clock();

	// Getting file size.
	if(!(fp = fopen(filename, "r"))) stop();
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	// Loading file contents.
	if(!(pointer = (char *)malloc(size + 1))) fclose(fp), stop();
	x = fread((void *)pointer, size, 1, fp);
	fclose(fp);
	if(!x) stop();

	// Interpreting lightscript.
	stats.inter = clock();
	light = lightscript(pointer);
	stats.output = clock();

	// Output.
	lightscript_output(light, stdout, options.colors);
	stats.end = clock();

	lightscript_free(light);
	free(pointer);
	return stats;
}

int main(int argc, char **argv)
{
	struct Options o;
	struct Stats stats;
	int i;

	o = parse_args(argc, argv);

	if(o.verbose)
	{
		fputs("Options:", stdout);
		if(o.stats)	fputs(" stats", stdout);
		if(o.verbose)	fputs(" verbose", stdout);
		if(o.colors)	fputs(" colors", stdout);
		fputc('\n', stdout);
	}

	for(i = 1; i < argc; i++)
	{
		if(!argv[i]) continue;
		stats = light(argv[i], o);
		if(o.stats) statistics(stats);
	}

	return 0;
}

void statistics(struct Stats stats)
{
	#define	level(n) ((n) < 100 ? 0 : (n) < 200 ? 1 : 2)

	const char *colors[] = { "\033[32;1m", "\033[34;1m", "\033[31;1m" };
	const char *end = "\033[0m";

	double t1 = (stats.inter - stats.begin)	* 1e6 / CLOCKS_PER_SEC;
	double t2 = (stats.output - stats.inter) * 1e6 / CLOCKS_PER_SEC;
	double t3 = (stats.end - stats.output) * 1e6 / CLOCKS_PER_SEC;

	int i1 = (int)t1;
	int i2 = (int)t2;
	int i3 = (int)t3;

	fprintf(stdout,
		"\nExecution statistics:\n"
		"\tData load\t%s%3d%s us\n"
		"\tInterpretation\t%s%3d%s us\n"
		"\tOutput\t\t%s%3d%s us\n",
		colors[level(i1)], i1, end,
		colors[level(i2)], i2, end,
		colors[level(i3)], i3, end
	);

	#undef	level
}

void stop(void)
{
	perror("error");
	exit(1);
}
