#ifndef	_LIGHTSCRIPT_H
#define	_LIGHTSCRIPT_H	1

// Provides size_t.
#include <stddef.h>
// Provides FILE.
#include <stdio.h>

struct Light;

struct Light *lightscript(const char *document);
struct Light *lightscript_size(const char *document, size_t size);
struct Light *lightscript_pointer(const char *document, const char *end);

void lightscript_output(struct Light *light, FILE *stream, int colors);
const char *lightscript_string(struct Light *light);

void lightscript_debug(struct Light *light);

void lightscript_free(struct Light *light);

#endif	// _LIGHTSCRIPT_H
