=== Test de lightscript ===

Texte, mais texte long : il faut que ça prenne de nombreuses lignes sinon ça ne
sert à rien que je me casse la tête à implémenter des analyses sur plusieurs
lignes. Cela dit, c'est bien quelque chose d'essentiel... il n'y a qu'à voir
les discussions de philo, ça génère des pavés assez impressionants... quid,
d'ailleurs, de la vitesse d'analyse sur des gros fichiers ?

. Titre de paragraphe : le style et les objets.
Le style de lightscript inclut /italique/, *gras*, _souligné_, et ~barré~. On
peut également insérer du `code` et des $expressions mathématiques$.

Smith >
	Ceci est une quote, factice de bout en bout. Juste là pour le test !
	Comme toute quote qui se respecte, elle peut contenir des
	sous-éléments :

	Dalton >
		Une sous-quote pour tester !

	. Sous-paragraphe
	La citation était vraiment longue !

	``` [plain]
	And what about indented bcode ?
	Well, what about it ?
	```

```
And what about plain bcode ?
Nah ?
```

Inventons un morceau de code bête pour illustrer les blocs de code indentés et
non-indentés, avec coloration syntaxique. Illustrons également les options :

	[C]
	int code_block(void)
	{
		int features = 0;

		features |= EMPTY_LINES;
		features |= MULTIPLE_INDENT;
		return features;
	}

``` [C]
#include <stdio.h>

int main(void)
{
	int i;
	for(i = 0; i < 10; i++) puts("Salut c'est moi !");
	puts("On a compris !");

	return 0;
}
```

Les spoilers permettent de masquer du contenu au cas où vous ayez des
révélations sensationnelles à faire sur le dernier jeu attendu.

((( Attention, spoil ! )))

	Un spoiler contient du texte formatable comme le reste du document.
	Voici par exemple du code Basic :

		[Basic]
		"Votre âge"?->A
		A<18=>"Vous êtes mineur"
		A>=18=>"Vous êtes majeur"

L'utilisation des listes à puces devrait être :

* Simple ;
* Complète ;
  * Donc inclure des sous-listes,
  * Et du `formatage`, /beaucoup/ de *formatage*.
* Et jolie.

* 0
* 1
	* 2
	* 3
* 4
		* 5
	* 6
* 7
			* 8
* 9
