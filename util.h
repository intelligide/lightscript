#ifndef	_UTIL_H
#define	_UTIL_H	1

// References : a fast and flexible way to analyze text without allocating any
// memory except the text itself.
typedef struct
{
	const char *begin;
	const char *end;
} reference_t;

#include <stdio.h>
void rprint(reference_t ref, FILE *stream);
void rprint_indent(reference_t ref, FILE *stream, int indent);



// Single text line manipulation.
const char *line_next(const char *line, const char *limit);
int line_isEmpty(const char *line, const char *limit);
int line_indent(const char *line, const char *limit);
int line_endsWith(const char *line, const char *line_end, int character,
	int count); // Non-empty lines assumed.
const char *line_skipIndent(const char *line, const char *limit, int levels);
	// Assumes that the lines has the given number of indentation levels.


#define	isspace(c) ((c) == ' ' || (c) == '\t' || (c) == '\n')
#define	refnull(ref) ((ref).begin >= (ref).end)

#endif	// _UTIL_H
