/*
	Heading definitions.

	[TODO]
		Inline formatting
		Auto-detect resource links
		Handle lists
*/

#include <stdlib.h>
#include "lightscript.h"
#include "analysis.h"
#include "util.h"
#include "output.h"



/*
	Main functions:

	lightscript()
	lightscript_size()
	lightscript_pointer()

	Translates the document into HTML.
*/

struct Light *lightscript(const char *document)
{
	const char *end = document;
	while(*end) end++;

	return lightscript_pointer(document, end);
}

struct Light *lightscript_size(const char *document, size_t size)
{
	return lightscript_pointer(document, document + size);
}

struct Light *lightscript_pointer(const char *document, const char *end)
{
	return parse(document, end, 0);
}



/*
	Output functions:

	lightscript_output()
	lightscript_string()

	Outputs a lightscript structure contents to an opened stream or a
	string, respectively.
*/

void lightscript_output(struct Light *light, FILE *stream, int colors)
{
	lightscript_output_node(light, stream, 0, colors);
}

const char *lightscript_string(struct Light *light)
{
	return NULL;
}



void lightscript_debug(struct Light *node)
{
	const char *names[] = {
		"none",
		"paragraph",
		"heading",
		"quote",
		"indented code",
		"backtick code",
		"spoiler",
		"list"
	};
	while(node)
	{
		puts(names[node->type]);
		node = node->next;
	}
}

/*
	lightscript_free()

	Frees memory.
*/

void lightscript_free(struct Light *node)
{
	struct Light *tmp;

	while(node)
	{
		if(node->type == bt_quote || node->type == bt_spoiler)
		{
			lightscript_free(node->child);
		}
		if(node->type == bt_list)
		{
			free(node->list.list_levels);
		}

		tmp = node->next;
		free(node);
		node = tmp;
	}
}
