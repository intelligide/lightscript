#ifndef	_ANALYZE_NODE_H
#define	_ANALYZE_NODE_H

#define	ANALYZE_NODE_PARAMS	\
	struct Light *node, const char *limit, int indent

extern const char *(*analyze_node[8])(ANALYZE_NODE_PARAMS);

#endif	// _ANALYZE_NODE_H
