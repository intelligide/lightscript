#include <stdlib.h>
#include "analysis.h"
#include "util.h"

enum Block_Type line_type(const char *line, const char *line_end, int indent)
{
	int length = line_end - line;
	int heading_level;
	const char *indent_skipped;

	if(line_isEmpty(line, line_end)) return bt_none;
	line = line_skipIndent(line, line_end, indent);

	// Handling indented code blocks.
	if(line[0] == '\t') return bt_icode;
	if(length >= 1 && line[0] == ' ' && line[1] == ' ') return bt_icode;

	// Skipping the first spaces.
	while(isspace(*line)) line++;
	length = line_end - line;

	// Handling non-indented code blocks.
	if(length >= 3 && line[0] == '`' && line[1] == '`' && line[2] == '`')
		return bt_bcode;

	// Trying to guess a heading.
	heading_level =
		(line[0] == '=') ? 1 :
		(line[0] == '-') ? 2 :
		(line[0] == '.') ? 3 :
		0;
	if(heading_level) return bt_h;

	// Maybe it's a blockquote ?
	if(line_endsWith(line, line_end, '>', 1)) return bt_quote;

	indent_skipped = line;
	while(isspace(*indent_skipped)) indent_skipped++;

	// Well, this could be a spoiler ?
	if(indent_skipped[0] == '(' && indent_skipped[1] == '('
		&& indent_skipped[2] == '('
		&& line_endsWith(line, line_end, ')', 3))
	{
		return bt_spoiler;
	}

	// That's right, I had forgotten about lists.
	if(indent_skipped[0] == '*') return bt_list;

	// I give up, there's things that definitely can't be interpreted.
	return bt_p;
}

struct Light *parse(const char *doc, const char *limit, int indent)
{
	struct Block_Answer block;
	struct Light *first = NULL, *last = NULL;

	while(1)
	{
		block = get_node(doc, limit, indent);
		if(!block.node) break;

		if(block.next_block <= doc)
		{
			fprintf(stderr, "\n\033[31;1mEmpty block returned."
				"\033[0m\n");
			abort();
		}

		// Constructing the linked list.
		if(!first) first = block.node;
		else last->next = block.node;
		last = block.node;

		doc = block.next_block;
	}

	return first;
}

struct Block_Answer get_node(const char *doc, const char *limit, int indent)
{
	struct Block_Answer answer = { NULL, NULL };
	struct Light *node;
	const char *first_end;

	// Skipping empty lines.
	while(doc < limit && line_isEmpty(doc, limit))
		doc = line_next(doc, limit);
	if(doc == limit) return answer;

	// Allocating a node.
	node = (struct Light *)malloc(sizeof(struct Light));
	if(!node) return answer;
	answer.node = node;
	node->next = NULL;

	// Getting the first line end and the block type.
	first_end = line_next(doc, limit);
	node->type = line_type(doc, first_end, indent);

// if(node->type == bt_list) node->type = bt_none;

	// Setting the block contents.
	node->contents.begin = doc;
	answer.next_block = (*analyze_node[node->type])(node, limit, indent);

	// Returning the block.
	return answer;
}
