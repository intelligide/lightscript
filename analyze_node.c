#include "analysis.h"
#include <stdlib.h>

/*
	analyze_node_*() requires the given node to have defined type and
	contents.begin.
	If the node appears to be a container, the child pointer is set in
	replacement of the contents reference.
*/

const char *analyze_node_none(ANALYZE_NODE_PARAMS)
{
	const char *next = line_next(node->contents.begin, limit);
	node->contents.end = next - 1;
	return next;
}

const char *analyze_node_p(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *next;

	while(block < limit)
	{
		if(line_indent(block, limit) < indent) break;
		if(line_isEmpty(block, limit)) break;

		next = line_next(block, limit);
		// Ensuring that the paragraph contains at least a line (this
		// is used, for instance, when a single quote opening line is
		// found without indented content).
		if(block != node->contents.begin)
		{
			if(line_type(block, next, indent) != bt_p) break;
		}

		block = next;
	}

	node->contents.end = block - 1;
	return block;
}

const char *analyze_node_h(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *next = NULL, *ret = NULL;

	int head;	// Heading character (one of =, -, .).
	int count = 0;	// Number of heading marks.

	// Getting the *real* title beginning, skipping the beginning marks.
	while(block < limit && (*block == ' ' || *block == '\t')) block++;
	head = *block;
	node->data.heading_level =
		(head == '=') ? 1 :
		(head == '-') ? 2 :
		3;

	while(block < limit && *block == head) block++, count++;
	while(block < limit && (*block == ' ' || *block == '\t')) block++;

	// Well, someone could have the crazy idea to put a lone title mark on
	// a line.
	if(block == limit || *block == '\n')
	{
		const char *real_end = block + (block != limit);
		node->type = bt_p;
		node->contents.end = real_end;
		return real_end;
	}

	node->contents.begin = block;

	// Headings with a single opening mark are single-line headings.
	if(count == 1)
	{
		next = line_next(block, limit);
		node->contents.end = next - 1;
		return next;
	}

	// Finding the first line that ends with the same heading mark.
	while(block < limit)
	{
		if(line_indent(block, limit) < indent) break;

		next = line_next(block, limit);
		if(line_endsWith(block, next, head, 1)) break;

		block = next;
	}

	// This is the actual value to be returned.
	ret = next;
	// Removing the last heading marks.
	while(--next > block && (isspace(*next) || *next == head));
	node->contents.end = next + 1;

	return ret;
}

const char *analyze_node_quote(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *contents = line_next(block, limit);

	while(contents < limit && line_isEmpty(contents, limit))
		contents = line_next(contents, limit);
	if(contents == limit || line_indent(contents, limit) < indent + 1)
	{
		node->type = bt_p;
		return analyze_node_p(node, limit, indent);
	}

	node->contents.begin = contents;
	block = contents;
	while(block < limit)
	{
		if(!line_isEmpty(block, limit)
			&& line_indent(block, limit) < indent + 1) break;

		block = line_next(block, limit);
	}

	node->contents.end = block - 1;
	node->child = parse(node->contents.begin, node->contents.end,
		indent + 1);

	return block;
}

const char *analyze_node_icode(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *last_nonempty_line;
	const char *param;

	// Trying to find an option specifier on the first line.
	param = line_skipIndent(block, limit, indent + 1);
	if(param == limit)
	{
		node->type = bt_none;
		node->contents.begin = node->contents.end = NULL;
		return line_next(block, limit);
	}
	if(*param == '[')
	{
		node->data.option.begin = param + 1;
		while(param < limit && *param != ']' && *param != '\n')
			param++;

		block = line_next(block, limit);

		if(param < limit && *param == ']')
		{
			node->contents.begin = block;
			node->data.option.end = param;
		}
		else node->data.option.end = NULL;
	}

	last_nonempty_line = limit;
	while(block < limit)
	{
		if(!line_isEmpty(block, limit))
		{
			if(line_indent(block, limit) < indent + 1) break;
			last_nonempty_line = block;
		}
		block = line_next(block, limit);
	}

	// Removing the empty lines that may have been caught at the end.
	node->contents.end = line_next(last_nonempty_line, limit) - 1;
	return block;
}

const char *analyze_node_bcode(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *next;
	const char *ptr;

	// Trying to find an option specifier (like [C] for syntax coloring).
	while(block < limit && (*block == ' ' || *block == '\t'
		|| *block == '`')) block++;
	if(block == limit)
	{
		node->type = bt_p;
		node->contents.end = limit;
		return limit;
	}

	// Isolating the option block.
	if(*block == '[')
	{
		const char *begin = block + 1;
		while(block < limit && *block != '\n' && *block != ']')
			block++;
		if(block < limit && *block == ']')
		{
			node->data.option.begin = begin;
			node->data.option.end = block;
		}
		else node->data.option.begin = node->data.option.end = NULL;
	}
	else node->data.option.begin = node->data.option.end = NULL;

	block = line_next(block, limit);
	node->contents.begin = block;

	// Getting the block end (first line that begins with three '`'. This
	// block handles setting node->contents.end because the line that
	// triggers loop end may have to be skipped if it contains backticks.
	while(block < limit)
	{
		if(line_indent(block, limit) < indent)
		{
			node->contents.end = block;
			break;
		}
		next = line_next(block, limit);
		ptr = line_skipIndent(block, next, indent);

		if(next - ptr > 3 && ptr[0] == '`' && ptr[1] == '`'
			&& ptr[2] == '`')
		{
			node->contents.end = block - 1;
			block = line_next(block, limit);
			break;
		}

		block = next;
	}

	return block;
}

const char *analyze_node_spoiler(ANALYZE_NODE_PARAMS)
{
	const char *block = node->contents.begin;
	const char *contents = line_next(block, limit);

	// Ensuring the spoiler isn't empty.
	while(contents < limit && line_isEmpty(contents, limit))
		contents = line_next(contents, limit);
	if(contents == limit || line_indent(contents, limit) < indent + 1)
	{
		node->type = bt_p;
		return analyze_node_p(node, limit, indent);
	}

	node->contents.begin = contents;
	block = contents;
	while(block < limit)
	{
		if(!line_isEmpty(block, limit)
			&& line_indent(block, limit) < indent + 1) break;

		block = line_next(block, limit);
	}

	node->contents.end = block - 1;
	node->child = parse(node->contents.begin, node->contents.end,
		indent + 1);

	return block;
}

const char *analyze_node_list(ANALYZE_NODE_PARAMS)
{
	struct Block_List list;
	const char *block = node->contents.begin;
	const char *line, *line_end;
	enum Block_Type type;
	int line_number = 0;
	int i;

	int current_indent;
	int *list_levels, *indent_levels;

	list.contents.begin = node->contents.begin;

	// Getting the end of the list.
	while(block < limit)
	{
		if(!line_isEmpty(block, limit)
			&& line_indent(block, limit) < indent) break;

		line_end = line_next(block, limit);
		type = line_type(block, line_end, indent);
		if(type != bt_list && type != bt_icode && type != bt_bcode)
			break;

		block = line_end;
		line_number++;
	}

	list_levels = (int *)malloc(line_number * sizeof(int));
	indent_levels = (int *)malloc(line_number * sizeof(int));

	// Getting the list of indent levels.
	line = node->contents.begin;
	for(i = 0; i < line_number && line < block; i++)
	{
		line = line_skipIndent(line, limit, indent);
		current_indent = 0;
		while(line < limit && *line != '\n')
		{
			if(*line == ' ') current_indent += 1;
			else if(*line == '\t')
				current_indent = (current_indent & ~7) + 8;
			else break;
			line++;
		}
		indent_levels[i] = current_indent;

		line = line_next(line, limit);
	}

	// Computing the list levels using the indent levels.
	list_levels[0] = 0;
	for(i = 1; i < line_number; i++)
	{
		int current = indent_levels[i];
		int last = indent_levels[i - 1];

		// If the current entry is on the same level than the previous.
		if(current == last || current == last + 1)
		{
			list_levels[i] = list_levels[i - 1];
			// Preventing increasing indentation.
			if(current == last + 1) indent_levels[i]--;
		}
		else if(current > last)
		{
			list_levels[i] = list_levels[i - 1] + 1;
		}
		else
		{
			// Finding the nearest accurate level.
			int j = i - 1;
			while(j > 0 && indent_levels[j] > current) j--;
			list_levels[i] = list_levels[j];
		}
	}

	/*
	printf("== List summary ==\n");
	printf("Number of lines : %d\n", line_number);
	printf("Level\tIndent\n----------------\n");
	for(i = 0; i < line_number; i++)
	{
		printf("%d\t%d\n", indent_levels[i], list_levels[i]);
	}
	printf("\n");
	*/

	free(indent_levels);

	list.list_levels = list_levels;
	list.contents.end = block - 1;
	node->list = list;
	return block;
}

const char *(*analyze_node[8])(ANALYZE_NODE_PARAMS) = {
	analyze_node_none,
	analyze_node_p,
	analyze_node_h,
	analyze_node_quote,
	analyze_node_icode,
	analyze_node_bcode,
	analyze_node_spoiler,
	analyze_node_list
};
