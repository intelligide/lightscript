.PHONY: all clear

src = lightscript.c util.c analysis.c analyze_node.c output.c
obj = $(addprefix build/, $(src:.c=.o))
hdr = lightscript.h util.h analysis.h analyze_node.h output.h
lib = liblightscript.so

cflags = -Wall -pedantic -std=c11 -Ofast -fPIC

all: build $(lib) light

build:
	mkdir -p build

$(lib): $(obj) $(hdr)
	gcc -shared -Wl,-soname,$(lib) -o $(lib) $(obj)
#	ar rcs $(lib) $(obj)

build/%.o: %.c
	gcc -c $^ -o $@ $(cflags)

light: main.c $(lib)
	gcc main.c -o light $(cflags) -L . -llightscript

install: all
	cp light /usr/local/bin

clean:
	rm -f $(obj)

fclean: clean
	rm -f $(lib) light

re: fclean all
