#ifndef	_ANALYSIS_H
#define	_ANALYSIS_H	1

#include "util.h"

/*
	Internal declarations.
*/

// A lightscript documented is interpreted as a list of block (nodes).
enum Block_Type
{
	bt_none		= 0,	// Empty blocks.

	bt_p		= 1,	// Common text paragraph.
	bt_h		= 2,	// Heading.
	bt_quote	= 3,	// Quotation block.
	bt_icode	= 4,	// Indented code block.
	bt_bcode	= 5,	// Backtick-marked code block.
	bt_spoiler	= 6,	// Spoiler block.
	bt_list		= 7	// List.
};

// Each block may have some meta data.
union Block_Data
{
	// Heading level : title (1), subtitle (2), paragraph title (3).
	int heading_level;

	// Quotation author or spoiler title.
	reference_t quote_author;
	reference_t spoiler_title;

	// List type : numbered or unordered.
	int list_numbered;

	// Generic option block.
	reference_t option;
};

struct Block_List
{
	reference_t contents;
	int *list_levels;
};

// This structure stores a block (node).
struct Light
{
	enum Block_Type type;
	union Block_Data data;

	// Some blocks contains raw text, others may contain other blocks
	// recursively.
	union
	{
		reference_t contents;
		struct Light *child;
		struct Block_List list;
	};

	// Using a linked list structure.
	struct Light *next;
};

// This structure is used by one of the functions.
struct Block_Answer
{
	struct Light *node;
	const char *next_block;
};

enum Block_Type line_type(const char *line, const char *line_end, int indent);
struct Light *parse(const char *doc, const char *limit, int indent);

struct Block_Answer get_node(const char *doc, const char *limit, int indent);

#include "analyze_node.h"

#endif	// _ANALYSIS_H
