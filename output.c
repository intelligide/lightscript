#include "output.h"
#include <stdarg.h>

struct Format
{
	const char *open;
	const char *close;
};
static int allow_colors = 0;

struct Format formats[] = {
	// None type.
	{
		"",
		""
	},
	// Paragraph.
	{
		"<p>",
		"</p>\n"
	},
	// Heading.
	{
		"<h%d>",
		"</h%d>\n"
	},
	// Blockquote.
	{
		"<blockquote>\n",
		"</blockquote>\n"
	},
	// Indented code.
	{
		"",
		""
	},
	// Backtick-code.
	{
		"",
		""
	},
	// Spoiler.
	{
		"<spoiler>\n",
		"</spoiler>\n"
	},
	// List.
	{
		"<ul>",
		"</ul>\n"
	}
};

static void open(enum Block_Type type, FILE *stream, ...)
{
	va_list args;
	va_start(args, stream);
	if(allow_colors) fprintf(stream, "\033[%d;1m", 30 + type);
	vfprintf(stream, formats[type].open, args);
	if(allow_colors) fputs("\033[0m", stream);
	va_end(args);
}

static void close(enum Block_Type type, FILE *stream, ...)
{
	va_list args;
	va_start(args, stream);
	if(allow_colors) fprintf(stream, "\033[%d;1m", 30 + type);
	vfprintf(stream, formats[type].close, args);
	if(allow_colors) fputs("\033[0m", stream);
	va_end(args);
}

static void indent(int level, FILE *stream)
{
	while(level)
	{
		fputc('\t', stream);
		level--;
	}
}

static void push(struct Light *light, int level, FILE *stream)
{
	reference_t ref = light->contents;
	ref.begin = line_skipIndent(ref.begin, ref.end, level);
	rprint(ref, stream);
}

// Output functions.

void output_node_none(OUTPUT_NODE_PARAMS)
{
}

void output_node_p(OUTPUT_NODE_PARAMS)
{
	open(bt_p, stream);
	push(light, level, stream);
	close(bt_p, stream);
}

void output_node_h(OUTPUT_NODE_PARAMS)
{
	open(bt_h, stream, light->data.heading_level);
	push(light, level, stream);
	close(bt_h, stream, light->data.heading_level);
}

void output_node_quote(OUTPUT_NODE_PARAMS)
{
	open(bt_quote, stream);
	lightscript_output_node(light->child, stream, level + 1, allow_colors);
	indent(level, stream);
	close(bt_quote, stream);
}

void output_node_icode(OUTPUT_NODE_PARAMS)
{
	fputs("<pre", stream);
	if(!refnull(light->data.option))
	{
		fputs(" data-lang='", stream);
		rprint(light->data.option, stream);
		fputc('\'', stream);
	}
	fputc('>', stream);

	rprint_indent(light->contents, stream, level + 1);
	fputs("</pre>\n", stream);
}

void output_node_bcode(OUTPUT_NODE_PARAMS)
{
	fputs("<pre", stream);
	if(!refnull(light->data.option))
	{
		fputs(" data-lang='", stream);
		rprint(light->data.option, stream);
		fputc('\'', stream);
	}
	fputc('>', stream);

	rprint_indent(light->contents, stream, level);
	fputs("</pre>\n", stream);
}

void output_node_spoiler(OUTPUT_NODE_PARAMS)
{
	open(bt_spoiler, stream);
	lightscript_output_node(light->child, stream, level + 1, allow_colors);
	indent(level, stream);
	close(bt_spoiler, stream);
}

void output_node_list(OUTPUT_NODE_PARAMS)
{
	open(bt_list, stream);
	push(light, level, stream);
	close(bt_list, stream);
}

void (*output_node[8])(OUTPUT_NODE_PARAMS) = {
	output_node_none,
	output_node_p,
	output_node_h,
	output_node_quote,
	output_node_icode,
	output_node_bcode,
	output_node_spoiler,
	output_node_list
};

void lightscript_output_node(OUTPUT_NODE_PARAMS, int colored)
{
	allow_colors = !!colored;

	while(light)
	{
		indent(level, stream);
		(*output_node[light->type])(light, stream, level);
		light = light->next;
	}
}
